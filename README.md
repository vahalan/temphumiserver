﻿# TempHumiServer

This is an example of a TCP/IP server implemented with Windows Forms
program.

The code is heavily based on the repository and video by Brian Ferguson.

https://github.com/AbleOpus/NetworkingSamples

https://www.youtube.com/watch?v=xgLRe7QV6QI

## About the TempHumiServer
The server listens for all IP adressess (`IPAddress.Any`) on port 3333, which can also be changed 
prior to pressing the listen button.

Upon connection the server sends the string "Hello" to the connecting client
and listens for messages from the client. The message structure of the
messages from client is the following:

| Type   | Remarks                                               |
|--------|-------------------------------------------------------|
| Int16  | Temperature in tenths of a degree, i.e. 251 = 25.1°C  |
| UInt16 | Humudity in tenths of percent, i.e. 800 = 80.0%       |

For example message with byte stream `0x03 0x01 0x22 0x03` will be 
intrepreted as:
```
Temperature: 25.9°C
Humidity:    80.2%
```

### Data format Class
Message formatting/serialization and parsing/deserialization is done by class 
Measurement in file Measurement.cs
