﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempHumiServer
{
    class Measurement
    {
        public Int16 Temperature { get; set; }
        public UInt16 Humidity { get; set; }

        public Measurement(Int16 temp, UInt16 hum)
        {
            Temperature = temp;
            Humidity = hum;
        }

        public Measurement(byte[] data)
        {
            Temperature = BitConverter.ToInt16(data, 0);
            Humidity = BitConverter.ToUInt16(data, 2);
        }

        /// <summary>
        ///  Serializes this package to a byte array.
        /// </summary>
        /// <remarks>
        /// Use the <see cref="Buffer"/> or <see cref="Array"/> class for better performance.
        /// </remarks>
        public byte[] ToByteArray()
        {
            List<byte> byteList = new List<byte>();
            byteList.AddRange(BitConverter.GetBytes(Temperature));
            byteList.AddRange(BitConverter.GetBytes(Humidity));
            return byteList.ToArray();
        }
    }
}
