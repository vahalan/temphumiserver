﻿using System;
using System.Drawing;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace TempHumiServer
{
    public partial class ServerForm : Form
    {
        private Socket serverSocket;
        private Socket clientSocket; // We will only accept one socket.
        private byte[] buffer;

        public ServerForm()
        {
            InitializeComponent();
        }

        private static void ShowErrorDialog(string message)
        {
            MessageBox.Show(message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        /// <summary>
        /// Construct server socket and bind socket to all local network interfaces, then listen for connections
        /// with a backlog of 10. Which means there can only be 10 pending connections lined up in the TCP stack
        /// at a time. This does not mean the server can handle only 10 connections. The we begin accepting connections.
        /// Meaning if there are connections queued, then we should process them.
        /// </summary>
        private void StartServer(int port)
        {
            try
            {
                serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                serverSocket.Bind(new IPEndPoint(IPAddress.Any, port));
                serverSocket.Listen(10);
                serverSocket.BeginAccept(AcceptCallback, null);
            }
            catch (SocketException ex)
            {
                ShowErrorDialog(ex.Message);
            }
            catch (ObjectDisposedException ex)
            {
                ShowErrorDialog(ex.Message);
            }
        }

        private void AcceptCallback(IAsyncResult AR)
        {
            try
            {
                clientSocket = serverSocket.EndAccept(AR);
                buffer = new byte[clientSocket.ReceiveBufferSize];

                var sendData = Encoding.ASCII.GetBytes("Hello");        // Send a message to the newly connected client.
                clientSocket.BeginSend(sendData, 0, sendData.Length,
                    SocketFlags.None, SendCallback, null);

                clientSocket.BeginReceive(buffer, 0, buffer.Length,
                    SocketFlags.None, ReceiveCallback, null);           // Listen for client data.
                serverSocket.BeginAccept(AcceptCallback, null);         // Continue listening for clients.
            }
            catch (SocketException ex)
            {
                ShowErrorDialog(ex.Message);
            }
            catch (ObjectDisposedException ex)
            {
                ShowErrorDialog(ex.Message);
            }
        }

        private void SendCallback(IAsyncResult AR)
        {
            try
            {
                clientSocket.EndSend(AR);
            }
            catch (SocketException ex)
            {
                ShowErrorDialog(ex.Message);
            }
            catch (ObjectDisposedException ex)
            {
                ShowErrorDialog(ex.Message);
            }
        }

        private void ReceiveCallback(IAsyncResult AR)
        {
            try
            {
                // Socket exception will raise here when client closes, as this sample does not
                // demonstrate graceful disconnects for the sake of simplicity.
                int received = clientSocket.EndReceive(AR);

                if (received == 0)
                {
                    return;
                }

                IPEndPoint ipEndpoint = (IPEndPoint)clientSocket.RemoteEndPoint;
                Measurement m = new Measurement(buffer);            // The received data is deserialized in the Measurement constructor
                DisplayMeasurement(ipEndpoint, m);

                clientSocket.BeginReceive(buffer, 0, buffer.Length, // Start receiving data again.
                    SocketFlags.None, ReceiveCallback, null);
            }
            // Avoid Pokemon exception handling in cases like these.
            catch (SocketException ex)
            {
                ShowErrorDialog(ex.Message);
            }
            catch (ObjectDisposedException ex)
            {
                ShowErrorDialog(ex.Message);
            }
        }

        /// <summary>
        /// Provides a thread safe way to add a row to the data grid.
        /// </summary>
        private void DisplayMeasurement(IPEndPoint ipEndpoint, Measurement m)
        {
            Invoke( (Action)delegate
            {
                float t = m.Temperature / 10.0f;
                float h = m.Humidity / 10.0f;
                rtBox.Text += "[" + ipEndpoint.Address.ToString() + ":" + ipEndpoint.Port.ToString() + "[ " + 
                              "temp: " + t.ToString("F1") + ", humi: " + h.ToString("F1") + "\n";
            });
        }

        private void btnListen_Click(object sender, EventArgs e)
        {
            UInt16 port = UInt16.Parse(tbPort.Text);
            StartServer(port);
            btnListen.Enabled = false;
            pbListen.BackColor = Color.Green;
        }

        private void tbListenAddr_TextChanged(object sender, EventArgs e)
        {
            tbListenAddr.Enabled = false;
            MessageBox.Show("Not in use, will listen to all IP's the machine has", "WIP");
        }

        private void tbListenAddr_Click(object sender, EventArgs e)
        {
            tbListenAddr_TextChanged(sender, e);
        }
    }

}
