﻿namespace TempHumiServer
{
    partial class ServerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtBox = new System.Windows.Forms.RichTextBox();
            this.btnListen = new System.Windows.Forms.Button();
            this.pbListen = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbListenAddr = new System.Windows.Forms.TextBox();
            this.tbPort = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbListen)).BeginInit();
            this.SuspendLayout();
            // 
            // rtBox
            // 
            this.rtBox.Location = new System.Drawing.Point(9, 53);
            this.rtBox.Name = "rtBox";
            this.rtBox.Size = new System.Drawing.Size(370, 285);
            this.rtBox.TabIndex = 2;
            this.rtBox.Text = "";
            // 
            // btnListen
            // 
            this.btnListen.Location = new System.Drawing.Point(188, 6);
            this.btnListen.Name = "btnListen";
            this.btnListen.Size = new System.Drawing.Size(75, 41);
            this.btnListen.TabIndex = 12;
            this.btnListen.Text = "Listen";
            this.btnListen.UseVisualStyleBackColor = true;
            this.btnListen.Click += new System.EventHandler(this.btnListen_Click);
            // 
            // pbListen
            // 
            this.pbListen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbListen.Location = new System.Drawing.Point(278, 6);
            this.pbListen.Name = "pbListen";
            this.pbListen.Size = new System.Drawing.Size(101, 41);
            this.pbListen.TabIndex = 10;
            this.pbListen.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Address:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Port:";
            // 
            // tbListenAddr
            // 
            this.tbListenAddr.Location = new System.Drawing.Point(60, 27);
            this.tbListenAddr.Name = "tbListenAddr";
            this.tbListenAddr.Size = new System.Drawing.Size(100, 20);
            this.tbListenAddr.TabIndex = 9;
            this.tbListenAddr.Text = "127.0.0.1";
            this.tbListenAddr.Click += new System.EventHandler(this.tbListenAddr_Click);
            this.tbListenAddr.TextChanged += new System.EventHandler(this.tbListenAddr_TextChanged);
            // 
            // tbPort
            // 
            this.tbPort.Location = new System.Drawing.Point(60, 6);
            this.tbPort.Name = "tbPort";
            this.tbPort.Size = new System.Drawing.Size(100, 20);
            this.tbPort.TabIndex = 7;
            this.tbPort.Text = "3333";
            // 
            // ServerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 373);
            this.Controls.Add(this.btnListen);
            this.Controls.Add(this.pbListen);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbListenAddr);
            this.Controls.Add(this.tbPort);
            this.Controls.Add(this.rtBox);
            this.Name = "ServerForm";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pbListen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtBox;
        private System.Windows.Forms.Button btnListen;
        private System.Windows.Forms.PictureBox pbListen;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbListenAddr;
        private System.Windows.Forms.TextBox tbPort;
    }
}

